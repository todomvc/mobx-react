import React from "react";
import { observer } from "mobx-react";
import { action } from "mobx";
import { Utils } from "./Utils";
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { TodoStore } from "./TodoStore";
import { ViewStore } from "./ViewStore";

export interface ITodoFooterProps {
  todoStore: TodoStore;
  viewStore: ViewStore;
}

@observer
export class TodoFooter extends React.Component<ITodoFooterProps> {
  render() {
    const todoStore = this.props.todoStore;
    if (!todoStore.activeTodoCount && !todoStore.completedCount) return null;

    const activeTodoWord = Utils.pluralize(todoStore.activeTodoCount, "item");

    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{todoStore.activeTodoCount}</strong> {activeTodoWord} left
        </span>
        <ul className="filters">
          {this.renderFilterLink(ALL_TODOS, "", "All")}
          {this.renderFilterLink(ACTIVE_TODOS, "active", "Active")}
          {this.renderFilterLink(COMPLETED_TODOS, "completed", "Completed")}
        </ul>
        {todoStore.completedCount === 0 ? null : (
          <button className="clear-completed" onClick={this.clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    );
  }

  renderFilterLink(filterName: string, url: string, caption: string) {
    return (
      <li>
        <a
          href={"#/" + url}
          className={
            filterName === this.props.viewStore.todoFilter ? "selected" : ""
          }
        >
          {caption}
        </a>{" "}
      </li>
    );
  }

  @action
  clearCompleted = () => {
    this.props.todoStore.clearCompleted();
  };
}
