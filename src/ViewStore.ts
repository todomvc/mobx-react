import { observable } from "mobx";
import { ALL_TODOS } from "./Constants";
import { TodoModel } from "./TodoModel";

export class ViewStore {
  @observable todoBeingEdited: TodoModel | null = null;
  @observable todoFilter: string = ALL_TODOS;
}
