import React from "react";
import ReactDOM from "react-dom";
import 'todomvc-common';
import { TodoApp } from "./TodoApp";
import * as serviceWorker from "./serviceWorker";
import { TodoStore } from "./TodoStore";
import { ViewStore } from "./ViewStore";

const todoStore = new TodoStore();
const viewStore = new ViewStore();

function render() {
  ReactDOM.render(
    <TodoApp todoStore={todoStore} viewStore={viewStore} />,
    document.getElementById("root")
  );
}

render();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
