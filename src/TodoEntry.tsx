import React from "react";
import ReactDOM from "react-dom";
import { observer } from "mobx-react";
import { action } from "mobx";
import { TodoStore } from "./TodoStore";

const ENTER_KEY = 13;

export interface ITodoEntryProps {
  todoStore: TodoStore;
}

@observer
export class TodoEntry extends React.Component<ITodoEntryProps> {
  render() {
    return (
      <input
        ref="newField"
        className="new-todo"
        placeholder="What needs to be done?"
        onKeyDown={this.handleNewTodoKeyDown}
        autoFocus={true}
      />
    );
  }

  @action
  handleNewTodoKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }

    event.preventDefault();

    let element = ReactDOM.findDOMNode(this.refs.newField) as HTMLInputElement;
    if (element) {
      const val = element.value.trim();
      if (val) {
        this.props.todoStore.addTodo(val);
        element.value = "";
      }
    }
  };
}
