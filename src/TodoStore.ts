import { observable, computed, reaction, action, IObservableArray } from "mobx";
import { TodoModel } from "./TodoModel";
import { Utils } from "./Utils";

export class TodoStore {
  @observable todos: IObservableArray<TodoModel> = observable.array([]);

  @computed
  get activeTodoCount() {
    return this.todos.reduce((sum, todo) => sum + (todo.completed ? 0 : 1), 0);
  }

  @computed
  get completedCount() {
    return this.todos.length - this.activeTodoCount;
  }

  subscribeServerToStore() {
    reaction(
      () => this.toJS(),
      todos =>
        window.fetch &&
        fetch("/api/todos", {
          method: "post",
          body: JSON.stringify({ todos }),
          headers: new Headers({ "Content-Type": "application/json" })
        })
    );
  }

  @action
  addTodo(title: string) {
    this.todos.push(new TodoModel(this, Utils.uuid(), title, false));
  }

  @action
  toggleAll(checked: boolean) {
    this.todos.forEach(todo => (todo.completed = checked));
  }

  @action
  clearCompleted() {
    this.todos = observable.array(this.todos.filter(todo => !todo.completed));
  }

  toJS() {
    return this.todos.toJS();
  }

  static fromJS(array: TodoModel[]) {
    const todoStore = new TodoStore();
    todoStore.todos = observable.array(
      array.map(item => TodoModel.fromJS(todoStore, item))
    );
    return todoStore;
  }
}
