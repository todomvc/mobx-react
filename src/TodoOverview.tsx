import React from "react";
import { observer } from "mobx-react";
import { ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { TodoItem } from "./TodoItem";
import { TodoStore } from "./TodoStore";
import { ViewStore } from "./ViewStore";

export interface ITodoOverviewProps {
  todoStore: TodoStore;
  viewStore: ViewStore;
}
@observer
export class TodoOverview extends React.Component<ITodoOverviewProps> {
  render() {
    const { todoStore, viewStore } = this.props;
    if (todoStore.todos.length === 0) return null;
    return (
      <section className="main">
        <input
          className="toggle-all"
          id="toggle-all"
          type="checkbox"
          onChange={this.toggleAll}
          checked={todoStore.activeTodoCount === 0}
        />
        <label htmlFor="toggle-all" />
        <ul className="todo-list">
          {this.getVisibleTodos().map(todo => (
            <TodoItem key={todo.id} todo={todo} viewStore={viewStore} />
          ))}
        </ul>
      </section>
    );
  }

  getVisibleTodos() {
    return this.props.todoStore.todos.filter(todo => {
      switch (this.props.viewStore.todoFilter) {
        case ACTIVE_TODOS:
          return !todo.completed;
        case COMPLETED_TODOS:
          return todo.completed;
        default:
          return true;
      }
    });
  }

  toggleAll = (event: React.ChangeEvent<HTMLInputElement>) => {
    const checked = event.target.checked;
    this.props.todoStore.toggleAll(checked);
  };
}
