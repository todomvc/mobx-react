import React from "react";
import { Router } from "director/build/director";
import { TodoFooter } from "./TodoFooter";
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import "./todomvc-app.css";
import "./todomvc-common.css";
import { observer } from "mobx-react";
import { TodoOverview } from "./TodoOverview";
import { TodoEntry } from "./TodoEntry";
import { TodoStore } from "./TodoStore";
import { ViewStore } from "./ViewStore";

export interface IAppProps {
  todoStore: TodoStore;
  viewStore: ViewStore;
}

export interface IAppState {
  editing?: string;
  nowShowing?: string;
}

@observer
class TodoApp extends React.Component<IAppProps, IAppState> {
  render() {
    const { todoStore, viewStore } = this.props;
    return (
      <div>
        <header className="header">
          <h1>todos</h1>
          <TodoEntry todoStore={todoStore} />
        </header>
        <TodoOverview todoStore={todoStore} viewStore={viewStore} />
        <TodoFooter todoStore={todoStore} viewStore={viewStore} />
      </div>
    );
  }

  componentDidMount() {
    const viewStore = this.props.viewStore;
    const router = new Router({
      "/": function() {
        viewStore.todoFilter = ALL_TODOS;
      },
      "/active": function() {
        viewStore.todoFilter = ACTIVE_TODOS;
      },
      "/completed": function() {
        viewStore.todoFilter = COMPLETED_TODOS;
      }
    });
    router.init("/");
  }
}

export { TodoApp };
